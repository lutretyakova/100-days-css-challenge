let isReady = false;

document.querySelectorAll('circle').forEach(item => (item.onclick = () => clickCircle()))

const clickCircle = () => {
	const circle = document.querySelector("#circle1")
		if (isReady) {
		circle.setAttribute('r', '18');
		circle.style.fill = 'navy';
		document.querySelectorAll('line').forEach((item) => {
			item.style.animation = ''
			item.style.dashOffset = 50
		})
	} else {
		circle.style.fill = 'lime';
		circle.setAttribute('r', '8');

		document.querySelectorAll('line').forEach((item) => {
			item.style.animation = 'dash 0.7s linear'
		})
	}
	
	isReady = !isReady;
}