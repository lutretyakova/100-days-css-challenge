window.onload = () => {
	document.querySelectorAll('li').forEach((item) => {
		item.classList.add('show')
	})
}

document.querySelector('.menu-btn').onclick = () => {
	if (document.querySelector('#menu').classList.value.includes('show')) {
		document.querySelector('#menu').classList.remove('show');	
	} else {
		document.querySelector('#menu').classList.add('show');	
	}
}