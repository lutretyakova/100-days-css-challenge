document.querySelector('#basic').onmouseenter = (event) => {
	document.querySelectorAll('.progress').forEach((item) => {
		item.style.width = '5%';	
	})
}

document.querySelector('#pro').onmouseenter = (event) => {
	document.querySelector('#users .progress').style.width = '30%';
	document.querySelector('#volume .progress').style.width = '70%';
	document.querySelector('#projects .progress').style.width = '50%'
}

document.querySelector('#premium').onmouseenter = (event) => {
	document.querySelectorAll('.progress').forEach((item) => {
		item.style.borderRadius = '5px';
		item.style.width = '100%';
	})
}

document.querySelectorAll('.tag').forEach(
	(item) => {
		item.onmouseleave = () => {
			document.querySelectorAll('.progress').forEach((item) => {
				item.style.borderRadius = '5px 0 0 5px';
				item.style.width = '0%';
			})
		}		
})