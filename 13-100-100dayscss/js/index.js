document.querySelectorAll('.plus').forEach( item => item.onclick = () => {
	document.querySelector('.top').classList.add('show');
	document.querySelector('.bottom').classList.add('show')
	document.querySelector('.photo img').classList.add('show')
	document.querySelector('.close').classList.add('show')
})

document.querySelectorAll('.close').forEach( item => item.onclick = () => {
	document.querySelector('.top').classList.remove('show');
	document.querySelector('.bottom').classList.remove('show')
	document.querySelector('.photo img').classList.remove('show')
	document.querySelector('.close').classList.remove('show')
})