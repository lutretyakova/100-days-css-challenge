document.querySelector("#send").onclick = (event) => {
	document.querySelector("#send").classList.remove("deactive");
	document.querySelector("#done").classList.remove("deactive");
	
	document.querySelector("#send").classList.add("active");
	document.querySelector("#done").classList.add("active");
}

document.querySelector("#done").onclick = (event) => {
	document.querySelector("#send").classList.remove("active");
	document.querySelector("#done").classList.remove("active");
	
	document.querySelector("#send").classList.add("deactive");
	document.querySelector("#done").classList.add("deactive");
}