document.querySelector('.center').onclick = (event) => {
	if (!event.target.classList.contains('circle')) {
		return
	}
	
	document.querySelectorAll('.circle').forEach(item => {
		if(item.classList.contains('deactive')) {
			item.classList.remove('deactive');
		}
		
		if(item.classList.contains('active')) {
			item.classList.remove('active');
			item.classList.add('deactive');
		}
	})
	event.target.classList.remove('deactive');
	event.target.classList.add('active');
}