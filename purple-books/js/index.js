const books = [
	{
		title: "Thief of Time",
		author:"Terry Pratchett",
		about: `The Auditors hire young clockmaker Jeremy Clockson to build a perfect glass clock, without telling him that this will stop time and thereby eliminate human unpredictability from the universe. Death discovers their plans...`
	},
	{
		title: "A Game of Thrones",
		author: "George R.R. Martin",
		about: `This novel launched the Song of Ice and Fire series and upended the established tropes of 1990s-era epic fantasy. Let’s not forget that shocking death at the end! `
	},
	{
		title: "The Hobbit",
		author: "J. R. R. Tolkien",
		about: `The Hobbit is set within Tolkien's fictional universe and follows the quest of home-loving hobbit Bilbo Baggins to win a share of the treasure guarded by Smaug the dragon. Bilbo's journey takes him from light-hearted, rural surroundings into more sinister territory. `
	}, 
	{
		title: "Starman Jones",
		author: "Robert A. Heinlein",
		about: `Max Jones works the family farm in the Ozark Mountains. With his father dead and his stepmother marrying again to a man he detests, Max runs away from home, taking his late uncle's astrogation manuals.`
	}
];

let counter = 1;

const setBookInfo = () => {
	document.querySelector(".count").innerHTML = counter;
	document.querySelector(".book").innerHTML = `<strong>${books[counter - 1].title}</strong>&nbsp;by ${books[counter - 1].author}`;
	document.querySelector(".about").innerHTML = `<p>${books[counter - 1].about}</p>`

	counter++;
	if (counter > books.length) {
		counter = 1
	}
}

window.onload = () => {
	setBookInfo()
}

document.querySelector("button").onclick = (event) => {	
	window.setTimeout(() => {
		document.querySelector(".block").classList.remove('active');
	}, 500)
	document.querySelector(".block").classList.add('active');
	setBookInfo()
}