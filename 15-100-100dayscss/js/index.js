 handleDownloadImages = (event) => {
        if (event.target.files && event.target.files[0]) {
            const reader = new FileReader();
            
            reader.onload = function (e) {
							const img = document.querySelector("#uploaded")
							console.log(img)
							img.setAttribute("src", e.target.result);
							img.classList.remove('hidden');
							document.querySelector("#file-upload").classList.add('hidden')
							document.querySelector(".btn-block").classList.remove('hidden')
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }
 
 document.querySelector("#upload-btn").onclick = (event) => {
	 document.querySelector('.progress-bar span').classList.add('activate')
	 window.setTimeout(() => { 
		 event.target.innerHTML = 'Done'
	 } , 2500)
 }