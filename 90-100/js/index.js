window.onload = () => {
		
		for (let inx = 1; inx < 20; inx ++) {
			const line = document.createElement('div');
			line.className = 'line left';
			line.style.left = 0;
			line.style.top = inx * 20 + 'px';
			document.querySelector('#board').append(line);
		}
	
		for (let inx = 1; inx < 20; inx ++) {
			const line = document.createElement('div');
			line.className = 'line right';
			line.style.right = 0;
			line.style.top = inx * 20 + 'px';
			document.querySelector('#board').append(line);
		}
}

document.querySelector('#board').onmousemove = (event) => {
	const boundRect = document.querySelector('#board').getBoundingClientRect();
	const mouseX = event.clientX - boundRect.left;
	const mouseY = event.clientY - boundRect.top; 
	
	const lineInx = Math.floor(mouseY / 20);
	
	const linesLeft = document.querySelectorAll('.left');
	const linesRight = document.querySelectorAll('.right');
	for (let inx = Math.max(lineInx - 20, 0); inx < Math.max(lineInx - 2, 0); inx++) {
			linesLeft[inx].style.width = 0;
			linesRight[inx].style.width = 0
	}
	
	for (let inx = Math.min(lineInx + 2, 19); inx < Math.min(lineInx + 20, 19); inx++) {
			linesLeft[inx].style.width = 0;
			linesRight[inx].style.width = 0
	}
	
	const widthLeft = [
		mouseX - 10, mouseX - 30, mouseX - 50, mouseX - 30, mouseX - 10
	];
	const widthRight = [
		400 - mouseX - 10, 
		400 - mouseX - 30, 
		400 - mouseX - 50, 
		400 - mouseX - 30, 
		400 - mouseX - 10
	];
	
	let inxW = lineInx - 3 >= 0 ? 0: Math.abs(lineInx - 3) 
	for (let inx = Math.max(lineInx - 3, 0); inx < Math.min(lineInx + 2, 19); inx++, inxW++) {
		linesLeft[inx].style.width = widthLeft[inxW] < 10 ? 0: widthLeft[inxW] + 'px';
		linesRight[inx].style.width = widthRight[inxW] < 10 ? 0: widthRight[inxW] + 'px'
	}
	
}

document.querySelector('#board').onmouseout = (event) => {
	document.querySelectorAll('.line').forEach(item => {
		item.style.width = 0
	})
}