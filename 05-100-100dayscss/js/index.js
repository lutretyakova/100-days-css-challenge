window.onload = () => {
	const x = [];
	for (let inx = 0; inx < 7; inx++) {
		step = (250 - 20) / 7;
		x.push(step / 2 + inx * step)
		console.log(step, x)
	}
	x.push(x[6])

	const yRed = [10, 15, 35, 15, 20, 12, 5, 5];
	const views = ['757', '720', '46', '720', '740', '725', '778'];
	for (let inx = 0; inx < 7; inx++) {
		const point = document.createElementNS('http://www.w3.org/2000/svg','circle');
		point.setAttribute('class','red');
		point.setAttribute('cx', x[inx]);
		point.setAttribute('cy', yRed[inx]);
		point.setAttribute('r', '3');
		
		const text = document.createElementNS('http://www.w3.org/2000/svg','text');
		text.setAttribute('x', x[inx]);
		text.setAttribute('y', yRed[inx]);
		
		const textNode = document.createTextNode(views[inx]);
		text.appendChild(textNode);
		
		const group = document.createElementNS('http://www.w3.org/2000/svg', 'g');
		group.appendChild(point);
		group.appendChild(text);
		
		document.querySelector("#red").append(group);
		
		const line = document.createElementNS('http://www.w3.org/2000/svg','line');
		line.setAttribute('class', 'red');
		line.setAttribute('x1', x[inx]);
		line.setAttribute('y1', yRed[inx]);
		line.setAttribute('x2', x[inx + 1]);
		line.setAttribute('y2', yRed[inx + 1]);
		$("#red").append(line);
	}
	
		const yBlue = [10, 20, 35, 10, 15, 8, 5, 5];
	for (let inx = 0; inx < 7; inx++) {
		const point = document.createElementNS('http://www.w3.org/2000/svg','circle');
		point.setAttribute('class','blue');
		point.setAttribute('cx', x[inx]);
		point.setAttribute('cy', yBlue[inx]);
		point.setAttribute('r', '3');
		$("#blue").append(point);
		
		const line = document.createElementNS('http://www.w3.org/2000/svg','line');
		line.setAttribute('class', 'blue');
		line.setAttribute('x1', x[inx]);
		line.setAttribute('y1', yBlue[inx]);
		line.setAttribute('x2', x[inx + 1]);
		line.setAttribute('y2', yBlue[inx + 1]);
		$("#blue").append(line);
	}
}