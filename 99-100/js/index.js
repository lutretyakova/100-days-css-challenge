document.querySelector('#btn').onclick = (event) => {
	document.querySelectorAll('.circle').forEach((item) => {
		if (item.classList.contains('active')) {
				item.classList.remove('active');
			  item.classList.add('deactive');
		} else {
				item.classList.remove('deactive');
				item.classList.add('active');
		}		
	})
	
	document.querySelectorAll('.line').forEach((item) => {
		if (item.classList.contains('active')) {
				item.classList.remove('active');
			  item.classList.add('deactive');
		} else {
				item.classList.remove('deactive');
				item.classList.add('active');
		}
	})
}